import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"
import { Type } from "./entity/Type"

AppDataSource.initialize().then(async () => {
    const productRepository = AppDataSource.getRepository(Product)
    const typeRepository = AppDataSource.getRepository(Type)
    const drink = await typeRepository.findOne({where: {name: 'drink'}})
    const bakery = await typeRepository.findOne({where: {name: 'bakery'}})
    const food = await typeRepository.findOne({where: {name: 'food'}})

    await productRepository.clear()


    var product = new Product()
    product.id = 1
    product.name = "Americano"
    product.price = 40
    product.type = drink
    await productRepository.save(product)

    var product = new Product()
    product.id = 2
    product.name = "Espresso"
    product.price = 50
    product.type = drink
    await productRepository.save(product)
    
    var product = new Product()
    product.id = 3
    product.name = "Coco"
    product.price = 50
    product.type = drink
    await productRepository.save(product)

    var product = new Product()
    product.id = 4
    product.name = "Cake 1"
    product.price = 70
    product.type = bakery
    await productRepository.save(product)

    var product = new Product()
    product.id = 5
    product.name = "Cake 2"
    product.price = 70
    product.type = bakery
    await productRepository.save(product)

    var product = new Product()
    product.id = 6
    product.name = "Som Tum"
    product.price = 70
    product.type = food
    await productRepository.save(product)

    const products = await productRepository.find({relations: {type: true}})
    console.log(products)
    
}).catch(error => console.log(error))
