import { AppDataSource } from "./data-source"
import { Order } from "./entity/Order"
import { OrderItem } from "./entity/OrderItem"
import { Product } from "./entity/Product"
import { User } from "./entity/User"

const orderDto = {
    orderItems: [
        {productId: 1, qty: 1},
        {productId: 2, qty: 1},
        {productId: 4, qty: 1},
    ],
    userId: 2,

}

AppDataSource.initialize().then(async () => {
    const userRepository = AppDataSource.getRepository(User)
    const productRepository = AppDataSource.getRepository(Product)
    const itemRepository = AppDataSource.getRepository(OrderItem)
    const orderRepository = AppDataSource.getRepository(Order)
    const user = await userRepository.findOneBy({id: orderDto.userId})

    const order = new Order()
    order.total = 0
    order.qty = 0
    order.user = user
    order.orderItems = []
    for(const oI of orderDto.orderItems) {
        const orderItem = new OrderItem()
        orderItem.product = await productRepository.findOneBy({id: oI.productId})
        orderItem.name = orderItem.product.name
        orderItem.price = orderItem.product.price
        orderItem.qty = oI.qty
        orderItem.total = orderItem.price * orderItem.qty
        await itemRepository.save(orderItem)
        order.orderItems.push(orderItem)
        order.total += orderItem.total
        order.qty += orderItem.qty
    }

    await orderRepository.save(order)

    const orders = await orderRepository.find({relations: {orderItems: true, user: true}})
    console.log(JSON.stringify(order, null, 2))

}).catch(error => console.log(error))
