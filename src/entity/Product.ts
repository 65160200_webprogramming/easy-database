import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, OneToMany } from "typeorm"
import { Type } from "./Type"
import { OrderItem } from "./OrderItem"

@Entity()
export class Product {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    price: number

    @CreateDateColumn()
    create: Date

    @UpdateDateColumn()
    updated: Date

    @ManyToOne(() => Type, (type) => type.products)
    type: Type

    @OneToMany(() => OrderItem, (item) => item.product)
    orderItems: OrderItem[]
}
