import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { OrderItem } from "./OrderItem";
import { User } from "./User";

@Entity()
export class Order {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    total: number

    @Column()
    qty: number

    @CreateDateColumn()
    create: Date

    @UpdateDateColumn()
    updated: Date

    @OneToMany(() => OrderItem, (item) => item.order)
    orderItems: OrderItem[]

    @ManyToOne(() => User, (user) => user.orders)
    user: any;
}
